import java.util.Vector;

/**
 * Created by svkreml on 24.09.16.
 */
public class TextToNumbers {
    static public String toString(int[] letters) {
        StringBuilder output = new StringBuilder();
        for (int i :
                letters) {
            if(i==27)
                output.append((char) (' '));
            else
            output.append((char) (i + 'a'));
        }
        return new String(output);
    }

    static public String toString(Vector<Integer> letters) {
        StringBuilder output = new StringBuilder();
        for (Integer i :
                letters) {
            if(i==27)
                output.append((char) (' '));
            else
            output.append((char) (i + 'a'));
        }
        return new String(output);
    }

    static public int[] toNum(String letters) {
        letters.toLowerCase();
        int[] output = new int[letters.length()];
        for (int i = 0; i < letters.length(); i++) {
            if(letters.charAt(i)==' ')
                output[i]=27;
            else
            output[i] = letters.charAt(i) - 'a';
        }
        return output;
    }
}
