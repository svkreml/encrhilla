import java.util.Vector;

/**
 * Created by svkreml on 24.09.16.
 */
public class EncrHilla {
    boolean logs = false;
    int module;
    int size;
    String key = "";
    Matrix K;


    EncrHilla(int module, int size, String key, boolean logs) {
        if (logs) System.out.println("size = " + size);
        if (logs) System.out.println("key.length() = " + key.length());
        if (size * size != (key.length()))
            throw new Error("key wrong");
        this.size = size;
        this.module = module;
        this.key = key;
        this.logs= logs;

        int[][] matrixKey = new int[size][];
        for (int i = 0; i < size; i++) {
            matrixKey[i] = TextToNumbers.toNum(key.substring(size * i, size * i + size));
        }
        K = new Matrix(matrixKey);
    }


    public String encrypt(String input) {
        Vector<Integer> in = new Vector<>();
        Vector<Matrix> encrypted = new Vector<>();
        if (logs) System.out.println("input = " + input);
        if (logs) System.out.println("key ");
        if (logs) K.print(2, 0);

        while (input.length() % size != 0)
            input += " ";
        for (int i = 0; i < input.length() / size; i++) {
            int[][] vector = new int[1][];
            vector[0] = TextToNumbers.toNum(input.substring(size * i, size * i + size));
            Matrix P = new Matrix(vector);
            if (logs) System.out.println("K-" + i);
            if (logs) K.print(3, 0);
            P = P.transpose();
            Matrix C = K.times(P);
            if (logs) System.out.println("C-" + i);
            if (logs) C.print(2, 0);
            C = modM(C, module);
            if (logs) System.out.println("C-mod " + module + ": " + i);
            if (logs) C.print(2, 0);
            encrypted.add(C);
            int[][] CArray = C.getArray();
            for (int l = 0; l < size; l++)
                in.add(CArray[l][0]);
        }
        if (logs) System.out.println(in);
        if (logs) System.out.println("encrypt = " + TextToNumbers.toString(in));
        //Расшифровка
        return TextToNumbers.toString(in);
    }

    public String decrypt(String encr) {
        Vector<Matrix> encrypted = new Vector<>();
        int[][] vector = new int[1][];
        for (int i = 0; i < encr.length() / size; i++) {
            vector[0] = TextToNumbers.toNum(encr.substring(size * i, size * i + size));
            Matrix C = new Matrix(vector);
            encrypted.add(C.transpose());
        }

        if (logs) System.out.println("Расшифровка");
        if (logs) System.out.println("key inverse");

        Matrix Ki = Adjugate(K);

        int det = K.det() % module;
        if (det == 0) throw new Error("Matrix invalid for some reason (det==0)");
        while (det < 0) det += module;
        if (logs) System.out.println("K.det() " + module + ": " + det % module);
        Gcd.GcdEx gcd = Gcd.gcdex(det, module);

        int mult = (gcd.x % module + module) % module;
        if (logs) System.out.println("mult = " + mult);
        Ki.timesEquals(mult);
        Ki = modM(Ki, module);
        if (logs) Ki.print(2, 0);

        Vector<Integer> out = new Vector<>();

        for (int i = 0; i < encr.length() / size; i++) {
            Matrix P = encrypted.get(i);
            Matrix C = Ki.times(P);
            if (logs) System.out.println("C-" + i);
            if (logs) C.print(2, 0);
            C = modM(C, module);
            if (logs) System.out.println("C-mod " + module + ": " + i);
            if (logs) C.print(2, 0);
            int[][] CArray = C.getArray();
            for (int l = 0; l < size; l++)
                out.add(CArray[l][0]);
        }
        if (logs) System.out.println(out);
        if (logs) System.out.println("output = " + TextToNumbers.toString(out));
        return TextToNumbers.toString(out);
    }

    static Matrix Adjugate(Matrix X) {
        int[][] matrix = X.getArray();
        int[][] output = new int[matrix.length][matrix.length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                output[i][j] = getMinor(X, i, j);
            }
        }
        return new Matrix(output);
    }

    static int getMinor(Matrix input, int row, int col) {
        Matrix X = input.transpose();
        int[][] matrix = X.getArray();
        int[][] minor = new int[matrix.length - 1][matrix.length - 1];
        int a = 0, b = 0;
        for (int i = 0; i < matrix.length; i++) {
            if (i != row) {
                for (int j = 0; j < matrix.length; j++) {
                    if (j != col) {
                        minor[a][b] = matrix[i][j];
                        b++;
                    }
                }
                a++;
                b = 0;
            }
        }
        Matrix m = new Matrix(minor);
        return (int) Math.pow(-1, col + row + 2) * m.det();
    }

    static Matrix modM(Matrix X, int m) {
        int[][] CArray = X.getArray();
        for (int row = 0; row < CArray.length; row++) {
            for (int col = 0; col < CArray[row].length; col++) {
                CArray[row][col] = CArray[row][col];
                while (CArray[row][col] < 0) CArray[row][col] += m;
                CArray[row][col] = CArray[row][col] % m;
            }
        }
        return new Matrix(CArray);
    }


}
